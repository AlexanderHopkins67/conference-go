import json
import pika
import django
import os
import sys
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:
        def process_approval(ch, method, properties, body):
            data = json.loads(body)
            print("  Received %r" % data.get("presenter_email"))
            send_mail(
                "Your presentation has been accepted",
                f"{data.get('presenter_name')}, we're happy to tell you that your presentation {data.get('title')} has been accepted",
                "Admin@conference.com",
                [data.get("presenter_email")],
            )

        def process_rejection(ch, method, properties, body):
            data = json.loads(body)
            print("  Received %r" % data.get("presenter_email"))
            send_mail(
                "Your presentation has been Rejected",
                f"{data.get('presenter_name')}, we're happy to tell you that your presentation {data.get('title')} has been accepted",
                "Admin@conference.com",
                [data.get("presenter_email")],
            )


        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue='presentation_approvals')
        channel.queue_declare(queue='presentation_rejections')
        channel.basic_consume(
            queue='presentation_approvals',
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.basic_consume(
            queue='presentation_rejections',
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.start_consuming()

    except AMQPConnectionError:
        print("Failed to connect to RabbitMQ")
        time.sleep(2.0)
