from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
import json
from common.encoder import ModelEncoder
from common.acls import get_weather

from .models import Conference, Location, State


@require_http_methods(["GET", "POST"])
@csrf_exempt
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        print(conferences)
        return JsonResponse({"conferences": conferences}, encoder=ConferenceListEncoder, safe=False)

    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            location = Location.objects.get(id=content.get("location"))
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"}, status=400)

        conference = Conference.objects.create(**content)
        return JsonResponse(conference, encoder=ConferenceDetailEncoder, safe=False)


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


@require_http_methods(["GET", "PUT", "DELETE"])
@csrf_exempt
def api_show_conference(request, id):
    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        city = conference.location.city
        state = conference.location.state.name

        return JsonResponse(
            {"weather": get_weather(city=city, state=state), "conference": conference},
            encoder=ConferenceDetailEncoder,
            safe=False
            )

    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            location = Location.objects.get(id=content.get("location"))
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"}, status=400)

        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(conference, encoder=ConferenceDetailEncoder, safe=False)

    else:
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
    ]

    def get_extra_data(self, o):
        return { "location": {"name": o.location.name, "href": o.location.get_api_url()}}


@require_http_methods(["GET", "POST"])
@csrf_exempt
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(locations, encoder=LocationListEncoder, safe=False)

    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        location = Location.create(**content)
        return JsonResponse(location, encoder=LocationDetailEncoder, safe=False)


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


@require_http_methods(["DELETE", "GET", "PUT"])
@csrf_exempt
def api_show_location(request, id):
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(location, encoder=LocationDetailEncoder, safe=False)

    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        Location.objects.filter(id=id).update(**content)

        location = Location.objects.get(id=id)
        location.set_img_url()
        return JsonResponse(location, encoder=LocationDetailEncoder, safe=False)


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url"
    ]

    def get_extra_data(self, o):
        return {
            "state": o.state.abbreviation,
            }
