from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_location_img(location):
    response = requests.get(
        url="https://api.pexels.com/v1/search",
        headers={'Authorization': PEXELS_API_KEY},
        params={
            "query": location,
            "per_page": 1
                },
        timeout=5
        )
    content = json.loads(response.content)
    return content.get("photos")[0].get("url")


def get_weather(city=None, state=None):
    response = requests.get(
        url="http://api.openweathermap.org/geo/1.0/direct",
        params={
            "q": {city, state},
            "appid": OPEN_WEATHER_API_KEY
        },
        timeout=5
    )
    lat = json.loads(response.content)[0].get("lat")
    lon = json.loads(response.content)[0].get("lon")

    r = requests.get(
        url="https://api.openweathermap.org/data/2.5/weather",
        params={
            "lat": lat,
            "lon": lon,
            "appid": OPEN_WEATHER_API_KEY,
            "units": "imperial"
        },
        timeout=5
    )
    temp = json.loads(r.content).get("main").get("temp")
    weather = json.loads(r.content).get("weather")[0].get("description")
    return ({"temp": f"{temp}F", "description": weather} if bool(r.content) else "Null")
