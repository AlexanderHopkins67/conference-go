from django.http import JsonResponse
import json
import pika
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods

from common.encoder import ModelEncoder
from events.models import Conference
from events.api_views import ConferenceListEncoder
from .models import Presentation


class PresentationsListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
    ]

    def get_extra_data(self, o):
        return {"status": o.status.name}


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference"
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}


@require_http_methods(["GET", "POST"])
@csrf_exempt
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        conference = Conference.objects.get(id=conference_id)
        p = Presentation.objects.filter(conference=conference)

        return JsonResponse({"presentations": p}, encoder=PresentationsListEncoder, safe=False)

    else:
        content = json.loads(request.body)
        conference = Conference.objects.get(id=conference_id)
        p = Presentation.create(**content)

        return JsonResponse(p, encoder=PresentationDetailEncoder, safe=False)


@require_http_methods(["GET", "PUT", "DELETE"])
@csrf_exempt
def api_show_presentation(request, id):
    if request.method == "GET":
        p = Presentation.objects.get(id=id)
        return JsonResponse(p, encoder=PresentationDetailEncoder, safe=False)

    elif request.method == "PUT":
        content = json.loads(request.body)
        Presentation.objects.filter(id=id).update(**content)
        p = Presentation.objects.get(id=id)
        return JsonResponse(p, encoder=PresentationDetailEncoder, safe=False)

    else:
        count, _ = Presentation.objects.get(id=id).delete()
        return JsonResponse({"deleted": count > 0})


@csrf_exempt
def update_status(request, id):
    if request.method == "PUT":
        presentation = Presentation.objects.get(id=id)
        data = json.loads(request.body)
        value = data["value"]
        presentation.change_status(value=value)
        return JsonResponse({"message": 200})


def send_status_message(queue, response):
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue=queue)
    channel.basic_publish(
    exchange="",
    routing_key=queue,
    body=response,
    )
    print("test")
    connection.close()


@require_http_methods(["PUT"])
@csrf_exempt
def api_approve_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.approve()
    response = json.dumps({
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title
    })

    send_status_message(
        queue="presentation_approvals",
        response=response
    )


    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
@csrf_exempt
def api_reject_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.reject()
    response = json.dumps({
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title
    })

    send_status_message(
        queue="presentation_rejections",
        response=response
    )
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )