from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
import json

from .models import Attendee, ConferenceVO, AccountVO
from common.encoder import ModelEncoder


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeesListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "email"
    ]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder,
    }

    def get_extra_data(self, o):
        count = AccountVO.objects.filter(email=o.email).count()
        return {"Has_account": count > 0}


@require_http_methods(["GET", "POST"])
@csrf_exempt
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)

        return JsonResponse({"attendees": attendees}, encoder=AttendeesListEncoder, safe=False)

    else:
        content = json.loads(request.body)
        try:
            conference_href = f'/api/conferences/{conference_vo_id}/'
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference

        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.create(**content)
        return JsonResponse(attendee, encoder=AttendeeDetailEncoder, safe=False)



@require_http_methods(["GET", "DELETE", "PUT"])
@csrf_exempt
def api_show_attendee(request, id):
    if request.method == "GET":
        person = Attendee.objects.get(id=id)
        return JsonResponse(person, encoder=AttendeeDetailEncoder, safe=False)

    elif request.method == "DELETE":
        count, _ = Attendee.objects.get(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        content = json.loads(request.body)
        Attendee.objects.filter(id=id).update(**content)
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(attendee, encoder=AttendeeDetailEncoder, safe=False)



@csrf_exempt
def create_badge(request, id):
    if request.method == "PUT":
        person = Attendee.objects.get(id=id)
        person.create_badge()
        return JsonResponse({"message": 200})
