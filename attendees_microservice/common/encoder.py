from json import JSONEncoder
from django.db.models import QuerySet
from datetime import datetime



# class ModelEncoder(JSONEncoder):
#     def __init__(self, excluded_fields=None, *args, **kwargs):
#         JSONEncoder.__init__(self)
#         self.excluded = excluded_fields

#     def datetime_encoder(self, value):
#         return value.isoformat()


#     def default(self, o):
#         encoders = {
#             "datetime": self.datetime_encoder,
#             "Badge": 1
#             }
#         properties = [f.name for f in o._meta.get_fields()]

#         d = {}
#         for p in properties:
#             value = getattr(o, p)
#             if value not in self.excluded:
#                 try:
#                     json.dumps(value)
#                     d[p] = value
#                 except TypeError:
#                     val = encoders[type(value).__name__](value)
#                     print(val)
#                     d[p] = val

#         return d


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
           return o.isoformat()
        else:
           return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            d = {}
            if hasattr(o, "get_api_url"):
                d["href"] = o.get_api_url()
            for property in self.properties:
                value = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]()
                    value = encoder.default(value)
                d[property] = value
            d.update(self.get_extra_data(o))
            return d
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}